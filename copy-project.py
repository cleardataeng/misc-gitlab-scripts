# Copyright 2019 ClearDATA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0

import argparse
import sys
import tempfile
import time

import gitlab

parser = argparse.ArgumentParser(description='Export a project by path')
parser.add_argument('path', type=str, help='path to source project')
parser.add_argument('--source-gitlab-cfg', type=str,
                    help='Name of source python-gitlab config')
parser.add_argument('--dest-gitlab-cfg', type=str,
                    help='Name of dest python-gitlab config')
parser.add_argument('--dest-namespace', type=str,
                    help='Dest namespace in dest gitlab')
args = parser.parse_args()

g = gitlab.Gitlab.from_config(args.source_gitlab_cfg)
h = gitlab.Gitlab.from_config(args.dest_gitlab_cfg)


def gen_dest_ns(new_parent, project):
    if new_parent:
        return '%s/%s' % (
            new_parent,
            project.attributes['namespace']['path'],
        )
    else:
        return project.attributes['namespace']['path']


def main():
    project = g.projects.get(args.path)

    print('Exporting project from source gitlab...')
    export = project.exports.create({})
    export.refresh()
    while export.export_status != 'finished':
        time.sleep(2)
        export.refresh()
        if export.export_status == 'failed':
            raise Exception('Export failed')

    with tempfile.TemporaryFile() as f:
        print('Downloading export...')
        export.download(streamed=True, action=f.write)
        f.seek(0)

        # check if it exists
        dest_ns = gen_dest_ns(args.dest_namespace, project)
        dest_full_path = '%s/%s' % (dest_ns, project.attributes['name'])
        try:
            h.projects.get(dest_full_path)
            print('destination project already exists')
            sys.exit(0)
        except gitlab.exceptions.GitlabGetError as e:
            if e.response_code == 404:
                pass            # project doesn't exist
            else:
                raise

        # import
        print('Importing into dest gitlab...')
        x = h.projects.import_project(f,
                                      project.attributes['name'],
                                      namespace=dest_ns)
        project_import = h.projects.get(x['id'], lazy=True).imports.get()
        while project_import.import_status != 'finished':
            time.sleep(2)
            project_import.refresh()
            if project_import.import_status == 'failed':
                raise Exception('Import failed')

    print('done!')


if __name__ == '__main__':
    main()
